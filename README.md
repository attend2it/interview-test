# README #

## attend2IT Interview Task. ##

### What is this repository for? ###

We'd like to see how you get on with something outside your comfort zone.  Also the idea would be for you to start coding in angular as well.  Therefore we would like you to perform the following task:-

* Implement a basic shopping basket application using the Angular framework, you can sell whatever you want.

* The application should contain a single page with two views; a list of products and a shopping basket.

* Users should be able to add products from the list in to the basket and subsequently be able to view these added products in a separate ‘basket’ list.

* Applicants should submit a zipped file containing the entire angular project source code which they have created in a development environment of their choice.

### How do I get set up? ###

`npm install`

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

This is the initial Angular scaffolding App. You will most likely want to reomve this intial HTML.

### Guidelines ###

* Spend no more than 90 minutes on this test
* You may include any other stlying or helper packages that you need

### Submission ###
* Ideally please create a new branch and push it back to this repo with your changes on.
* If this is not working please zip up you work **excluding the nodes_modules folder** and email it to kerry@attend2it.co.uk

### Who do I talk to? ###

* Any problems or questions then please email kerry@attend2it.co.uk